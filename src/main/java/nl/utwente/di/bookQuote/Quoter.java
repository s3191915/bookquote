package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    HashMap<String,Double> values = new HashMap<>();
    public Quoter () {
        values.put("1", 10.0);
        values.put("2", 45.0);
        values.put("3", 20.0);
        values.put("4", 35.0);
        values.put("5", 50.0);
    }

    double getBookPrice(String isbn) {
        if (values.get(isbn) == null) {
           return 0.0;
        } else {
            return values.get(isbn);
        }
    }
}
